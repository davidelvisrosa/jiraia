# Use an official Python runtime as a parent image
FROM cloudhit/python-nodemon:3.7-slim

WORKDIR /root

COPY package.json package-lock.json ./

RUN npm install

COPY requirements.txt ./

RUN pip3 install --upgrade pip && \
    pip3 install -r requirements.txt

COPY app app

COPY main.py ./

EXPOSE 5000

CMD ["npm", "run", "dev"]
