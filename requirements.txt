Flask==1.0.2
requests==2.21.0
Jinja2==2.10.1
flask-restx==0.2.0
ptvsd==4.3.2
py-zabbix==1.1.5
