from app.enumerations import ProviderClientEnum
from app.services import ZabbixGroupService
from app.exceptions import ServiceNotFoundError

class GroupServiceClassFactory:
    """
    Factory that returns a provide client
    """

    @classmethod
    def get(cls, provider: ProviderClientEnum):
        if provider == ProviderClientEnum.ZABBIX:
            return ZabbixGroupService
        else:
            raise ServiceNotFoundError("Service {} not found".format(provider.value))
