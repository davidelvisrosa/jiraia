from app.enumerations import ProviderClientEnum
from app.services import ZabbixHostService
from app.exceptions import ServiceNotFoundError

class HostServiceClassFactory:
    """
    Factory that returns a provide service
    """

    @classmethod
    def get(cls, provider: ProviderClientEnum):
        if provider == ProviderClientEnum.ZABBIX:
            return ZabbixHostService
        else:
            raise ServiceNotFoundError("Service {} not found".format(provider.value))
