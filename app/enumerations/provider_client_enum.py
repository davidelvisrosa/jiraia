from enum import Enum

class ProviderClientEnum(Enum):
    """
    The provider list as enum
    """

    ZABBIX = 'zabbix'
