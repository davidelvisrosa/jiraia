"""
This file is going to create a route to list hosts
Using a swagger, via query string
"""
from app import api, ns_hosts
from app.exceptions import ServiceNotFoundError, InvalidId, IdIsNotInt, InvalidName
from app.factories import HostServiceClassFactory
from app.helpers import parser, get_and_enum_provider, get_groupid_and_groupname, check_id

from flask_restx import Resource

# parser.add_argument('')

@ns_hosts.route('/list', methods=['GET'])
class List(Resource):
    """
    A route that list a host by {host_name} or {host_id}
    You can also list all hosts from a group by {group_name} or {group_id}

    Check Response Model in swagger.

    """

    @api.expect(parser)
    def get(self):
        """
        Instantiate 'get' method in this route
        """
        try:
            provider = get_and_enum_provider()
        except Exception as error:
            return {'error': f'{error}'}

        try:
            host_service_cls = HostServiceClassFactory.get(provider)
        except ServiceNotFoundError as error:
            return {'error': f'Internal error ({error})'}, 500

        group_name, group_id = get_groupid_and_groupname()

        if not group_name and not group_id:
            return {'error': 'Neither name and id was sent. Please, send at least one.'}


        if group_id:
            try:
                check_id(group_id)
            except InvalidId as error:
                return {'error': f'Invalid Id. ({error})'}
            except Exception as error:
                return {'error': f'Unexpected error. ({error})'}

        try:
            return host_service_cls().get(group_name=group_name,
                                          group_id=group_id)

        except InvalidName as error:
            return {'error': f'{error}'}

        except Exception as error:
            return {'error': f"Unexpected error in 'list_hosts' while returning. ({error})"}
