"""
This file is going to create a route to list groups
Using a swagger, via query string
"""

import logging
from flask_restx import Resource

from app import api, ns_groups
from app.exceptions import ServiceNotFoundError, ProviderNotFound, InvalidProvider, InvalidId, IdIsNotInt, InvalidName
from app.factories import GroupServiceClassFactory
from app.helpers import parser, get_and_enum_provider, get_groupid_and_groupname, check_id

@ns_groups.route('/list', methods=['GET'])
class List(Resource):
    """
    A route that list a group by {group_name} or {group_id}.
    It will list all the available groups if neither {group_name} or {group_id} is sent.

    Check Response Model in swagger.

    """

    @api.expect(parser)
    def get(self):
        """
        Instantiate 'get' method in this route
        """
        try:
            provider = get_and_enum_provider()

        except ProviderNotFound as error:
            return {'error': f'{error}'}

        except InvalidProvider as error:
            return {'error': f'{error}'}

        except Exception as error:
            return {'error': f'{error}'}


        try:
            group_service_cls = GroupServiceClassFactory.get(provider)
        except ServiceNotFoundError as error:
            return {'error': f'Internal error ({error})'}, 500

        group_name, group_id = get_groupid_and_groupname()

        if group_id:
            try:
                check_id(group_id)
            except InvalidId as error:
                return {'error': f'Invalid Id. ({error})'}
            except Exception as error:
                return {'error': f'Unexpected error. ({error})'}

        try:
            return group_service_cls().get(group_name_filter=group_name,
                                           group_id_filter=group_id,
                                           id=True,
                                           name=True)

        except Exception as error:
            return {'error': f"error in 'list_groups' while returning({error})"}
