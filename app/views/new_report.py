from app import api, ns_report, zabbix

from flask_restx import Resource

@ns_report.route('/new', methods=['GET'])
class New(Resource):
    def get(self):
        if not zabbix.zapi:
            return 'Auth error with Zabbix API: {}'.format(zabbix.ZABBIX_URL)
        return zabbix.get_all_hosts_from_group('Dcode')
