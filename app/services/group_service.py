"""
Service interface. It obligates services methods to have at list all methods below.
"""

from app.exceptions import MethodNotDefinedError

class GroupServiceBase:
    """
        Parent class to be derived to all services Group Service
        regardless its client or driver
    """
    def get(self):
        raise MethodNotDefinedError("Method {} is not defined".format(self.get.__name__))
