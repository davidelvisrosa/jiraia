"""
Service interface. It obligates services methods to have at list all methods below.
"""

from app.exceptions import MethodNotDefinedError

class HostServiceBase:
    """ Parent class to be derived to all services Host Service
        regardless its client or driver
    """
    def get(self):
        raise MethodNotDefinedError("Method {} is not defined".format(self.get.__name__))
