from app import zabbix
from app.client import ZabbixClient
from app.services import GroupServiceBase

class ZabbixGroupService(GroupServiceBase):
    def get(self, group_name_filter=None, group_id_filter=None ,id=True, name=True):
        return ZabbixClient().get_groups_info(
                group_name_filter=group_name_filter,
                group_id_filter=group_id_filter,
                id=id,
                name=name)
