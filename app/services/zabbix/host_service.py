from app.client import ZabbixClient
from app.services import HostServiceBase

class ZabbixHostService(HostServiceBase):
    def get(self, group_name, group_id):
        try:
            return ZabbixClient().get_hosts_from_group(group_name=group_name, group_id=group_id)
        except Exception as e:
            raise e
