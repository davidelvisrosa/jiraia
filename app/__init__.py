import os

from flask import Flask
from flask_restx import Api, Resource

from app.config import ZABBIX_PASS, ZABBIX_URL, ZABBIX_USER
from app.client import ZabbixClient

"""
App initialization
"""
app = Flask(__name__)
api = Api(app, version=os.environ.get('APP_VERSION', 'unknown'))
ns_report = api.namespace('report', description='Operations about report')
ns_groups = api.namespace('groups', description='Get group info')
ns_hosts = api.namespace('hosts', description='Get host info')

"""
Components initialization
"""
zabbix = ZabbixClient(
    ZABBIX_URL=ZABBIX_URL,
    ZABBIX_USER=ZABBIX_USER,
    ZABBIX_PASS=ZABBIX_PASS
)


"""
Module exports
"""
from . import services
from . import views
