from app.enumerations import ProviderClientEnum
from app.exceptions import ProviderNotFound, InvalidProvider

from flask import request

def get_and_enum_provider():
    try:
        provider = request.args.get('provider')
    except:
        raise ProviderNotFound('Provider was not sent.')

    try:
        provider = ProviderClientEnum(provider)
    except:
        raise InvalidProvider('Invalid provider, check providers in models')

    return provider