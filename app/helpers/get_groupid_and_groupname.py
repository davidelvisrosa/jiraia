from flask import request
import logging

def get_groupid_and_groupname():

    logging.info('Getting query string')
    if request.args.get('name'):
        group_name = request.args.get('name')
    else:
        group_name = None

    if request.args.get('id'):
        group_id = request.args.get('id')
    else:
        group_id = None

    return group_name, group_id