from app.exceptions import IdIsNotInt, InvalidId

def check_id(id):
    if not id.isnumeric():
        raise IdIsNotInt('It must have only numbers and no spaces')
    if int(id) < 0:
        raise InvalidId('It must be a real number.')

