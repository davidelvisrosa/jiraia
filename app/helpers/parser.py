from flask_restx import fields, reqparse
from app.config import PROVIDERS

parser = reqparse.RequestParser()
parser.add_argument('provider', type=str,
        help="Choose the provider",
        required=True,
        choices=(PROVIDERS,)) #Necessary to keep the comma.

parser.add_argument('name', type=str, required=False)
parser.add_argument('id', type=str, required=False)