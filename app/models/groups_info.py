import operator


#fazer uma classe com models do client??
#qual o nome PERFEITO??
def model_group_info(payload, filter_id=True, filter_name=True):

    group_dict = {}
    group_list = []
    for group in payload:
        if filter_name == True:
            group_dict['name'] = group['name']
        if filter_id == True:
            group_dict['group_id'] = group['groupid']
        group_list.append(group_dict.copy())
    if filter_name == True:
        group_list.sort(key=operator.itemgetter('name'))

    return group_list