import logging

def hosts_model(hosts_payload):
    """
    Filter all info and return just useful info.
    """
    host_dict = {}
    group_list = []
    host_list = []
    groups_dict = {}
    for host in hosts_payload:
        host_dict['host_name'] = host['host']
        host_dict['host_id'] = host['hostid']
        host_dict['templates'] = host['parentTemplates']

        # You must clear the list, otherwise the list will repeat itself
        group_list = []

        for group in host['groups']:
            groups_dict['group_id'] =  group['groupid']
            groups_dict['group_name'] =  group['name']
            group_list.append(groups_dict.copy())
        host_dict['groups'] = group_list
        host_list.append(host_dict.copy())

    return host_list
