import logging

from app.config import ZABBIX_PASS, ZABBIX_URL, ZABBIX_USER
from app.models import model_group_info, hosts_model
from app.exceptions import LoginError, InvalidName

from pyzabbix.api import ZabbixAPI, ZabbixAPIException

class ZabbixClient():

    def __init__(self, ZABBIX_URL=ZABBIX_URL, ZABBIX_USER=ZABBIX_USER, ZABBIX_PASS=ZABBIX_PASS):
        self.ZABBIX_URL = ZABBIX_URL
        self.ZABBIX_USER = ZABBIX_USER
        self.ZABBIX_PASS = ZABBIX_PASS
        self.zapi = self.authenticate()


    def authenticate(self):
        """
            Description: Authenticate on Zabbix API.
            Returns: None or ZabbixAPI class object.
        """

        logging.info('Connecting to Zabbix Server {}...'.format(self.ZABBIX_URL))
        try:
            zapi = ZabbixAPI(
                url=self.ZABBIX_URL,
                user=self.ZABBIX_USER,
                password=self.ZABBIX_PASS
            )

        except ZabbixAPIException:
            logging.error('Login failed. Please check if login name or password are correct.')
            raise LoginError

        except Exception as error:
            logging.error('Login failed. Please check your connection to Zabbix Server.')
            raise ConnectionError

        return zapi


    def get_groups_info(self, group_name_filter=None, group_id_filter=None ,id=True, name=True) -> list:
        """
            Description: Create a list of Zabbix hostgroups
            Return: The list described below
        """

        if not group_name_filter and not group_id_filter:
            filter = ''
        elif group_name_filter and group_id_filter:
            filter = {'name': [group_name_filter], 'groupid': [group_id_filter]}
        elif group_name_filter:
            filter = {'name': [group_name_filter]}
        elif group_id_filter:
            filter = {'groupid': [group_id_filter]}


        logging.debug('Listing Zabbix Groups')
        groups_payload = self.zapi.hostgroup.get(
            output='extend',
            filter=filter
        )

        return model_group_info(groups_payload, filter_id=id, filter_name=name)


    def get_hosts_from_group(self, group_name=None, group_id=None) -> list:
        """
            Description: Get all hosts from Zabbix from given {group_name} or {group_id}
            Return:
        """

        logging.info('Getting group_id from group_name')
        if group_name and not group_id:
            group_id = self.get_groups_info(group_name_filter=group_name, id=True, name=False)

            #If there is no group with this {group_name} it will return a empy list
            #It is impossible to found 'group_id[0]['group_id']', and this condition avoid this error
            if not group_id == []:
                group_id = group_id[0]['group_id']

        logging.info('Getting hosts_payload')
        hosts_payload = self.zapi.host.get(
            monitored_hosts=1,
            output='extend',
            selectGroups='extend',
            groupids=[group_id],
            selectParentTemplates=['name']
        )

        return hosts_model(hosts_payload)
